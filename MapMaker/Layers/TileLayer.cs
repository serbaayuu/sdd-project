﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProgram.Layers
{
    class TileLayer : Layer
    {
        Tile[,] _tileGrid;

        public override void Initialize(LayerType layerType, int mapHeight, int mapWidth)
        {
            _layerType = layerType;

            _tileGrid = new Tile[mapHeight, mapWidth];
            int tileIterator = 0;
            for (int x = 0; x < mapHeight; x++)
            {
                for (int y = 0; y < mapWidth; y++)
                {
                    Tile newTile = new Tile(0, x, y);
                    _tileGrid[x, y] = newTile;
                }
            }
        }

        public void SetTile(int xPos, int yPos, Tile tile)
        {
            _tileGrid[xPos, yPos] = tile;
        }

        public void DeleteTile(int xPos, int yPos)
        {
            _tileGrid[xPos, yPos] = new Tile(0, xPos, yPos);
        }
    }
}
