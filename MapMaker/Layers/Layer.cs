﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProgram.Layers
{
    class Layer
    {
        public enum LayerType
        {
            TileLayer = 0,
            ObjectLayer = 1,
            RegionLayer = 2,
        }
        public LayerType _layerType;

        public virtual void Initialize()
        {
            
        }
    }
}
