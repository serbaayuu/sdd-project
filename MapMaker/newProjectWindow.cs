﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestProgram
{
    public partial class newProjectWindow : Form
    {
        Map map;

        public newProjectWindow(ref Map _map)
        {
            InitializeComponent();
            map = _map;
        }

        private void newProjectWindow_Load(object sender, EventArgs e)
        {
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            map.mapName = mapNameBox.Text;
            map.tileWidth = Convert.ToInt32(tileBoxX.Text);
            map.tileHeight = Convert.ToInt32(tileBoxY.Text);
            map.mapWidth = Convert.ToInt32(mapBoxX.Text);
            map.mapHeight = Convert.ToInt32(mapBoxY.Text);

            this.Close();
        }
    }
}
