﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TestProgram
{
    public class Map
    {
        public int _tileWidth;
        public int _tileHeight;
        public int _mapWidth;
        public int _mapHeight;
        public string _mapName;

        //The tileset broken up into a list of tiles. There is only one tileset per map.
        List<Tile> _tileList;
        //The list of layers in the project. The index of the layer indicates draw priority (0 is the bottom most layer)
        List<Layer> _layers;

        Image _tilesetImage; 
        //Tile[,] _tileList;

        public void Initialize(int tileSize, int mapWidth, int mapHeight, string mapName)
        {
            _tileWidth = tileSize;
            _tileHeight = tileSize;
            _mapWidth = mapWidth;
            _mapHeight = mapHeight;
            _mapName = mapName;

            _layers = new List<Layer>();
            _tileList = new List<Tile>();
            /*
            _tileList = new Tile[_mapHeight, _mapWidth];
            int tileIterator = 0;
            for (int x = 0; x < _mapHeight; x++)
            {
                for (int y = 0; y < _mapWidth; y++)
                {

                }
            }
            */
        }

        void SetTileList(Image tileset)
        {
            _tilesetImage = tileset;
            int tileColumns = _tilesetImage.Width / _tileWidth;
            int tileRows = _tilesetImage.Height / _tileHeight;

        }

        void CreateNewTileLayer()
        {

        }
        void CreateNewObjectLayer()
        {

        }
        void CreateNewRegionLayer()
        {

        }
    }
}
