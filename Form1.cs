﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestProgram
{
    public partial class Form1 : Form
    {
        Map _map = new Map();

        public Form1()
        {
            InitializeComponent();
        }

        private void newProjectButton_Click(object sender, EventArgs e)
        {
            newProjectWindow newProjWin = new newProjectWindow(ref _map);
            newProjWin.Show();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveWin = new SaveFileDialog();
            saveWin.Filter = "MAPMAKER File|*.mmkr";
            saveWin.Title = "Save Project";
            saveWin.ShowDialog();
            if (saveWin.FileName != "")
            {
                System.IO.FileStream fs = (System.IO.FileStream)saveWin.OpenFile();

                // code goes here to place data into file

                fs.Close();

            }
        }

        
    }
}
